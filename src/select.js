var oracledb = require('oracledb');
var http = require('http');
var url = require('url');
var querystring = require('querystring');

var db = null;
oracledb.getConnection(
	{
		user          : "mmonteil",
		password      : "password",
		connectString : "LFR010923/xe"
	},
	function(err, connection) {
		if (err) {
			console.error(err);
			return;
		}
		db = connection;
	}
);


var server = http.createServer(function(req, res) {
	// Url page
	var page = url.parse(req.url).pathname;
	// Url params
	var params = querystring.parse(url.parse(req.url).query);

	res.writeHead(200, {"Content-Type": "text/html"});
	res.write('<!DOCTYPE html>'+
	'<html>'+
	'    <head>'+
	'        <meta charset="utf-8" />'+
	'        <title>Ma page Node.js !</title>'+
	'    </head>'+ 
	'    <body>');

	// Prefix supported
	var wsPrefix = '/webservice';

	if (page.indexOf(wsPrefix) == 0) {
		var service = page.substring(wsPrefix.length);
		console.log('Service called : ' + service);
		// Find User
		if (service == '/findUser') {
			console.log('The service findUser exists');
			if ('usr_id' in params) {
				var userId = params['usr_id'];
				// DB query
				db.execute(
					"SELECT * FROM PIY_USERS " +
					"WHERE id = :id",
					[userId],
					function(err, result) {
						if (err) {
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
							return;
						}
						console.log(result);
						for (var i = 0; i < result.metaData.length; i++) {
							var col = result.metaData[i].name;
							var val = result.rows[0][i].toString();
							res.write('<p>' + col + ' : ' + val + '</p>');
						}
						res.write('</body></html>');
						res.end();
					}
				);
			} else {
				res.write('<p>Unvalid param !</p>');
				res.write('</body></html>');
				res.end();
			}

		}
		// Find Catalog
		else if (service == '/findCatalog') {
			console.log('The service findCatalog exists');
			if ('cat_id' in params) {
				var cat_id = params['cat_id'];
				// DB query
				db.execute(
					"SELECT * FROM PIY_CATALOGS " +
					"WHERE id = :id",
					[cat_id],
					function(err, result) {
						
						console.log(result);
						
						if (err) {
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
							return;
						} else if (!result.rows[0]) {
							res.write('<p>Does not exist !</p>');
							res.write('</body></html>');
							res.end();
							return;
						} else {
							for (var i = 0; i < result.metaData.length; i++) {
								var col = result.metaData[i].name;
								if (!result.rows[0][i]) {
									var val = '';
								} else {
									var val = result.rows[0][i].toString();
								}
								res.write('<p>' + col + ' : ' + val + '</p>');
							}
							res.write('</body></html>');
							res.end();
						}
					}
				);
			} else {
				res.write('<p>Unvalid param !</p>');
				res.write('</body></html>');
				res.end();
			}

		}
		// Create Catalog
		else if (service == '/createCatalog') {
			console.log('The service createCatalog exists');
			var allOk = true;

			// ID
			var id = null;
			if ('id' in params) {
				id = params['id'];
			} else {
				allOk = false;
			}

			// LABEL
			var label = null;
			if ('label' in params) {
				var label = params['label'];
			} else {
				allOk = false;
			}

			// FK_AC_SERIE
			var serie = null;
			if ('serie' in params) {
				var serie = params['serie'];
			} else {
				allOk = false;
			}
			
			// FK_STD_SPEC
			var spec = null;
			if ('spec' in params) {
				var spec = params['spec'];
			} else {
				allOk = false;
			}

			console.log('id : ' + id + ' label : ' + label + ' serie : ' + serie + ' spec : ' + spec);

			if (allOk) {
				
				var query = "INSERT INTO PIY_CATALOGS " +
					"(ID, LABEL, FK_AC_SERIE, FK_STD_SPEC) " +
					"VALUES (:id, :label, :serie, :spec)";

				// DB query
				db.execute(
					query,
					[id, label, serie, spec],
					function(err, result) {
						if (err) {
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
							return;
						} else {
							console.log('log1');
							db.commit();
							console.log('log2');
							res.write('<p>Catalog created !</p>');
							res.write('</body></html>');
							res.end();
						}
					}
				);
			} else {
				res.write('<p>A param missing !</p>');
				res.write('</body></html>');
				res.end();
			}

		} else {
			res.write('<p>Unvalid service !</p>');
			res.write('</body></html>');
			res.end();
		}
	// res.writeHead(404, {"Content-Type": "text/plain"});
	}
});

server.listen(8081);