----------------
-- DROP TABLE --
----------------

DROP TABLE PIY_VERSIONS;
DROP TABLE PIY_CONFIGURATIONS;
DROP TABLE PIY_CONFIG_ETATS;
DROP TABLE PIY_PROJECT;
DROP TABLE PIY_CLUSTER_LVL;
DROP TABLE PIY_USERS;
DROP TABLE PIY_ROLES;
DROP TABLE PIY_AIRLINES;
DROP TABLE PIY_CATALOGS;
DROP TABLE PIY_STD_SPECIFICATIONS;
DROP TABLE PIY_CLASSE_TYPES;
DROP TABLE PIY_AIRCRAFT_SERIES;

------------------
-- CREATE TABLE --
------------------

CREATE TABLE PIY_ROLES (
	code VARCHAR (10) NOT NULL,
	label VARCHAR (150),
	PRIMARY KEY (code)
);

CREATE TABLE PIY_AIRLINES (
	id INT NOT NULL,
	icao_code VARCHAR (10),
	label VARCHAR (50),
	logo_path VARCHAR (255),
	PRIMARY KEY (id)
);

CREATE TABLE PIY_USERS (
	id INT NOT NULL,
	login VARCHAR (50),
	pwd VARCHAR (50),
	name VARCHAR (50),
	fk_role VARCHAR (10),
	fk_airline INT,
	PRIMARY KEY (id),
	FOREIGN KEY (fk_role) REFERENCES PIY_ROLES(code),
	FOREIGN KEY (fk_airline) REFERENCES PIY_AIRLINES(id)
);

CREATE TABLE PIY_AIRCRAFT_SERIES (
	id INT NOT NULL,
	label VARCHAR (150),
	json BLOB,
	PRIMARY KEY (id)
);

CREATE TABLE PIY_CLASSE_TYPES (
	id INT NOT NULL,
	label VARCHAR (150),
	img_path VARCHAR (255),
	fk_ac_serie INT,
	PRIMARY KEY (id),
	FOREIGN KEY (fk_ac_serie) REFERENCES PIY_AIRCRAFT_SERIES(id)
);

CREATE TABLE PIY_STD_SPECIFICATIONS (
	code VARCHAR (50) NOT NULL,
	rank VARCHAR (3),
	ref VARCHAR (20),
	iss VARCHAR (2),
	rev VARCHAR (2),
	fk_ac_serie INT,
	PRIMARY KEY (code),
	FOREIGN KEY (fk_ac_serie) REFERENCES PIY_AIRCRAFT_SERIES(id)
);

CREATE TABLE PIY_CATALOGS (
	id INT NOT NULL,
	label VARCHAR (150),
	json BLOB,
	fk_std_spec VARCHAR (50),
	PRIMARY KEY (id),
	FOREIGN KEY (fk_std_spec) REFERENCES PIY_STD_SPECIFICATIONS(code)
);

CREATE TABLE PIY_CLUSTER_LVL (
	id INT NOT NULL,
	label VARCHAR (150),
	PRIMARY KEY (id)
);

CREATE TABLE PIY_PROJECT (
	id INT NOT NULL,
	name VARCHAR (150),
	creation_date DATE,
	json BLOB,
	fk_customer INT,
	fk_project_leader INT,
	fk_operator INT,
	fk_plane INT,
	fk_cluster_lvl INT,
	PRIMARY KEY (id),
	FOREIGN KEY (fk_customer) REFERENCES PIY_AIRLINES(id),
	FOREIGN KEY (fk_project_leader) REFERENCES PIY_USERS(id),
	FOREIGN KEY (fk_operator) REFERENCES PIY_USERS(id),
	FOREIGN KEY (fk_plane) REFERENCES PIY_AIRCRAFT_SERIES(id),
	FOREIGN KEY (fk_cluster_lvl) REFERENCES PIY_CLUSTER_LVL(id)
);

CREATE TABLE PIY_CONFIG_ETATS (
	code VARCHAR (3) NOT NULL,
	label VARCHAR (150),
	PRIMARY KEY (code)
);

CREATE TABLE PIY_CONFIGURATIONS (
	id INT NOT NULL,
	json BLOB,
	fk_etat VARCHAR (3),
	fk_project INT,
	PRIMARY KEY (id),
	FOREIGN KEY (fk_etat) REFERENCES PIY_CONFIG_ETATS(code),
	FOREIGN KEY (fk_project) REFERENCES PIY_PROJECT(id)
);

CREATE TABLE PIY_VERSIONS (
	id INT NOT NULL,
	name VARCHAR(255),
	etat VARCHAR(150),
	creation_date DATE,
	json BLOB,
	fk_config INT,
	PRIMARY KEY (id),
	FOREIGN KEY (fk_config) REFERENCES PIY_CONFIGURATIONS(id)
);

-- INSERT INTO
INSERT INTO PIY_CONFIG_ETATS (code, label) VALUES ('INI', 'Initialized');
INSERT INTO PIY_CONFIG_ETATS (code, label) VALUES ('WIP', 'Work in progress');
INSERT INTO PIY_CONFIG_ETATS (code, label) VALUES ('VAL', 'Validated');
INSERT INTO PIY_CONFIG_ETATS (code, label) VALUES ('CAN', 'Cancelled');

INSERT INTO PIY_ROLES (code, label) VALUES ('AL', 'Airline');
INSERT INTO PIY_ROLES (code, label) VALUES ('CT', 'Customer Account');
INSERT INTO PIY_ROLES (code, label) VALUES ('ADMIN', 'Admin');

INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (1, 'JSJ', 'JS AIR (PRIVATE) LIMITED');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (2, '', 'JOHN HOLLAND AVIATION SERVICES');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (3, '', 'LOCAL GOVERNMENT OF MADEIRA');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (4, '', 'FLY MI - AVIATION AND BUSINESS');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (5, '', 'DHL AVIATION N.V/S.A');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (6, '', 'SANTANDER');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (7, '', 'AEROCONSEIL AIRCRAFT ENGINEERING');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (8, '', 'PRIVATE CUSTOMER 33');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (9, '', 'MID EAST JET');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (10, '', 'AMAC AEROSPACE SWITZERLAND AG');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (11, '', 'SAYWELL INTERNATIONAL INC.');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (12, '', 'PAB AIRWING');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (13, 'WAU', 'WIZZ AIR UKRAINE');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (14, '', 'AEROSPACE DESIGN AND ENGINEERING CONSULTANTS LTD');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (15, '', 'SKY HOLDING COMPANY, LLC');
INSERT INTO PIY_AIRLINES (id, icao_code, label) VALUES (16, '', 'PRIVATE CUSTOMER 34');

INSERT INTO PIY_USERS (id, login, pwd, name, fk_role, fk_airline) VALUES (1, 'login_ay', 'password', 'ayyoub', 'CT', 1);
INSERT INTO PIY_USERS (id, login, pwd, name, fk_role, fk_airline) VALUES (2, 'login_an', 'password', 'anass', 'ADMIN', 2);
INSERT INTO PIY_USERS (id, login, pwd, name, fk_role, fk_airline) VALUES (3, 'login_al', 'password', 'alexis', 'AL', 3);
INSERT INTO PIY_USERS (id, login, pwd, name, fk_role, fk_airline) VALUES (4, 'login_ma', 'password', 'matthieu', 'AL', 4);
INSERT INTO PIY_USERS (id, login, pwd, name, fk_role, fk_airline) VALUES (5, 'login_em', 'password', 'emmanuel', 'ADMIN', 5);

INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (1, 'F4-600ST');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (2, '330-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (3, '330-300');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (4, '330-400');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (5, '330-500');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (6, '340-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (7, '340-300');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (8, '340-300C');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (9, '340-500');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (10, '340-600');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (11, '318-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (12, '319-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (13, '320-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (14, '320-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (15, '321-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (16, '321-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (17, '380-700');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (18, '380-800');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (19, '380-800F');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (20, '380-800R');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (21, '380-900');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (22, '380-XXX');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (23, '310-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (24, '310-200C');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (25, '310-200V');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (26, '310-300');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (27, '310-300F');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (28, 'A300-600ST');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (29, 'B1');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (30, 'B2-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (31, 'B2-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (32, 'B2-300');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (33, 'B4-100');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (34, 'B4-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (35, 'B4-600');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (36, 'B4-600R');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (37, 'C4-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (38, 'C4-600');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (39, 'C4-600R');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (40, 'F4-200');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (41, 'F4-600R');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (42, '350-900');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (43, '330-200F');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (44, '380-800Z');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (45, '350-800');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (46, '350-1000');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (47, '330-200K');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (48, '350-900S');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (49, '320-200N');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (50, '321-200N');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (51, '319-100N');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (52, '330-800');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (53, '330-900');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (54, '330-700L');
INSERT INTO PIY_AIRCRAFT_SERIES (id, label) VALUES (55, '321-200NX');

INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (1, '320-200_ECO', './img/eco-class.png', 14);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (2, '320-200_BUSINESS', './img/business-class.png', 14);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (3, '320-200_FIRST', './img/first-class.png', 14);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (4, '318-100_ECO', './img/eco-class.png', 11);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (5, '318-100_BUSINESS', './img/business-class.png', 11);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (6, '318-100_FIRST', './img/first-class.png', 11);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (7, '380-800_ECO', './img/eco-class.png', 18);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (8, '380-800_BUSINESS', './img/business-class.png', 18);
INSERT INTO PIY_CLASSE_TYPES (id, label, img_path, fk_ac_serie) VALUES (9, '380-800_FIRST', './img/first-class.png', 18);

INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_001', 11, '001', 'P 000 01000', 'A', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_002', 11, '002', 'P 000 01000', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_003', 11, '003', 'P 000 01000', '01', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_004', 12, '001', 'J 000 01000', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_005', 12, '002', 'J 000 01000', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_006', 12, '003', 'J 000 01000', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_007', 12, '005', 'J 000 01000', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_008', 12, '006', 'J 000 01000CJ', 'B', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_009', 12, '007', 'J 000 01000', '04', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_010', 13, '001', 'D 000 20100', 'E', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_011', 13, '002', 'D 000 20100', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_012', 13, '003', 'D 000 20100', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_013', 13, '004', 'D 000 01501', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_014', 14, '001', 'D 000 02200', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_015', 14, '003', 'D 000 02000', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_016', 14, '005', 'D 000 02102', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_017', 14, '007', 'D 000 02000', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_018', 14, '008', 'D 000 02102', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_019', 14, '009', 'D 000 02102', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_020', 14, '010', 'D 000 02000', '03', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_021', 14, '011', 'D 000 02000', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_022', 14, '012', 'D 000 02000', '05', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_023', 14, '013', 'D 000 02000', '05', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_024', 15, '001', 'E 000 01000', 'P', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_025', 15, '002', 'E 000 01000', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_026', 15, '004', 'E 000 01000', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_027', 15, '005', 'E 000 01000', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_028', 15, '006', 'E 000 01000', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_029', 15, '007', 'E 000 01000', '05', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_030', 16, '001', 'E 000 02000', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_031', 16, '002', 'E 000 02000', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_032', 16, '003', 'E 000 02000', '02', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_033', 11, '004', 'P 000 01000', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_034', 12, '008', 'J 000 01000', '05', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_035', 14, '014', 'D 000 02000', '06', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_036', 16, '004', 'E 000 02000', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_037', 11, '005', 'P 000 01000', '03', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_038', 12, '009', 'J 000 01000', '06', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_039', 14, '015', 'D 000 02000', '07', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_040', 16, '005', 'E 000 02000', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_041', 11, '006', 'P 000 01000E', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_042', 12, '010', 'J 000 01000CJ', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_043', 14, '016', 'D 000 02000', '08', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_044', 12, '011', 'J 000 01000', '07', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_045', 16, '006', 'E 000 02000', '05', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_046', 11, '007', 'P 000 01000E', '02', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_047', 11, '008', 'P 000 01000', '04', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_048', 49, '001', 'D 000 02000N', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_049', 50, '001', 'E 000 02000N', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_050', 51, '001', 'J 000 01000N', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_051', 50, '002', 'E 000 02000N', 'B', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_052', 49, '002', 'D 000 02000N', 'B', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_053', 51, '002', 'J 000 01000N', 'B', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_054', 49, '003', 'D 000 02000N', '01', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_055', 51, '003', 'J 000 01000N', '01', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_056', 12, '012', 'J 000 01000', '07', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_057', 14, '017', 'D 000 02000', '08', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_058', 16, '007', 'E 000 02000', '05', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_059', 55, '002', 'E 000 02000NX', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_060', 55, '001', 'E 000 02000NX', 'B', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_061', 50, '003', 'E 000 02000N', '01', '01');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_062', 51, '004', 'J 000 01000NJ', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_063', 49, '004', 'D 000 02000NJ', '01', '00');
INSERT INTO PIY_STD_SPECIFICATIONS (code, fk_ac_serie, rank, ref, iss, rev) VALUES ('STD_SPEC_064', 55, '003', 'E 000 02000NX', '01', '01');

-- Pas top de mettre SPEC + AIRCRAFT etant donne que SPEC contient AIRCRAFT
INSERT INTO PIY_CATALOGS (id, label, fk_std_spec) VALUES (1, 'Catalog été 2015', 'STD_SPEC_001');
INSERT INTO PIY_CATALOGS (id, label, fk_std_spec) VALUES (2, 'Catalog hiver 2015', 'STD_SPEC_001');
INSERT INTO PIY_CATALOGS (id, label, fk_std_spec) VALUES (3, 'Catalog été 2016', 'STD_SPEC_001');
INSERT INTO PIY_CATALOGS (id, label, fk_std_spec) VALUES (4, 'Catalog hiver 2016', 'STD_SPEC_001');

INSERT INTO PIY_CLUSTER_LVL (id, label) VALUES (0, 'Basic');
INSERT INTO PIY_CLUSTER_LVL (id, label) VALUES (1, 'Standard');
INSERT INTO PIY_CLUSTER_LVL (id, label) VALUES (2, 'Plus');
INSERT INTO PIY_CLUSTER_LVL (id, label) VALUES (3, 'Complet');

INSERT INTO PIY_PROJECT (id, name, creation_date, fk_customer, fk_project_leader, fk_operator, fk_plane, fk_cluster_lvl)
VALUES (1, 'Projet X', '01/07/17', 1, 5, 2, 1, 1);
INSERT INTO PIY_PROJECT (id, name, creation_date, fk_customer, fk_project_leader, fk_operator, fk_plane, fk_cluster_lvl)
VALUES (2, 'Plane It Yourself', '21/07/17', 3, 5, 2, 1, 1);

INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (1, 'INI', 1);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (2, 'VAL', 1);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (3, 'CAN', 1);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (4, 'WIP', 1);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (5, 'INI', 2);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (6, 'VAL', 2);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (7, 'CAN', 2);
INSERT INTO PIY_CONFIGURATIONS (id, fk_etat, fk_project) VALUES (8, 'WIP', 2);

INSERT INTO PIY_VERSIONS (id, name, etat, creation_date, fk_config) VALUES (1, 'Amendment 1', 'INI', '01/07/16', 2);
INSERT INTO PIY_VERSIONS (id, name, etat, creation_date, fk_config) VALUES (2, 'Commercial Freeze 1', 'INI', '01/10/16', 2);
INSERT INTO PIY_VERSIONS (id, name, etat, creation_date, fk_config) VALUES (3, 'Commercial Freeze 2', 'VAL', '22/01/17', 2);
INSERT INTO PIY_VERSIONS (id, name, etat, creation_date, fk_config) VALUES (4, 'Amendment 2', 'VAL', '05/02/17', 2);

