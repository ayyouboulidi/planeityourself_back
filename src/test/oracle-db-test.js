var oracledb = require('oracledb');
var http = require('http');
var url = require('url');
var querystring = require('querystring');

var db = null;
oracledb.getConnection(
	{
		user          : "mmonteil",
		password      : "password",
		connectString : "LFR010923/xe"
	},
	function(err, connection) {
		if (err) {
			console.error(err);
			return;
		}
		db = connection;
	}
);

var server = http.createServer(function(req, res) {
	console.log('-------------------------------');
	// Msg
	var koPath = 'Unknown path !';
	var koParam = 'Unvalid param !';
	var koService = 'Unknown service !';
	var koFound = 'Not found !';
	var okService = 'This service exists !';
	var okCreation = 'Created !';
	var okSuccess = 'Success !';
	var okCommit = 'Commited !';
	// Url page
	var page = url.parse(req.url).pathname;
	// Url params
	var params = querystring.parse(url.parse(req.url).query);
	// Supported prefix
	var wsPrefix = '/webservice';

	if (page.indexOf(wsPrefix) == 0) {
		// HTTP 200
		res.writeHead(200, {"Content-Type": "text/html"});
		res.write('<!DOCTYPE html>'+
		'<html>'+
		'    <head>'+
		'        <meta charset="utf-8" />'+
		'        <title>Ma page Node.js !</title>'+
		'    </head>'+ 
		'    <body>');

		// Called service
		var service = page.substring(wsPrefix.length);
		console.log('Called service : ' + service);

		// Find User
		if (service == '/findUser') {
			console.log(okService);
			// Param ?
			if ('usr_id' in params) {
				var userId = params['usr_id'];
				// DB query
				var query = "SELECT * FROM PIY_USERS WHERE id = :id";
				console.log('query : ' + query);
				console.log('id : ' + userId);

				// Execute query
				db.execute(query, [userId],
					function(err, result) {
						console.log(result);
						if (err) {
							// Error
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
						} else if (!result.rows[0]) {
							// Empty
							console.log(koFound);
							res.write('<p>' + koFound + '</p>');
							res.write('</body></html>');
							res.end();
						} else {
							// Success
							for (var i = 0; i < result.metaData.length; i++) {
								var col = result.metaData[i].name;
								var val = '';
								if (!!result.rows[0][i]) {
									val = result.rows[0][i].toString();
								}
								res.write('<p>' + col + ' : ' + val + '</p>');
							}
							res.write('</body></html>');
							res.end();
						}
					}
				);
			} else {
				console.log(koParam);
				res.write('<p>' + koParam + '</p>');
				res.write('</body></html>');
				res.end();
			}

		}
		// Find ALL User
		if (service == '/findAllUser') {
			console.log(okService);
			// DB query
			var query = "SELECT * FROM PIY_USERS";
			console.log('query : ' + query);

			// Execute query
			db.execute(query, [],
				function(err, result) {
					console.log(result);
					if (err) {
						// Error
						console.error(err);
						res.write('<p>' + err + '</p>');
						res.write('</body></html>');
						res.end();
					} else if (!result.rows[0]) {
						// Empty
						console.log(koFound);
						res.write('<p>' + koFound + '</p>');
						res.write('</body></html>');
						res.end();
					} else {
						// Success
						for (var i = 0; i < result.rows.length; i++) {
							res.write('<p>-------------------------</p>');
							for (var j = 0; j < result.metaData.length; j++) {
								var col = result.metaData[j].name;
								var val = '';
								if (!!result.rows[i][j]) {
									val = result.rows[i][j].toString();
								}
								res.write('<p>' + col + ' : ' + val + '</p>');
							}
						}
						res.write('</body></html>');
						res.end();
					}
				}
			);
		}
		// Find Catalog
		else if (service == '/findCatalog') {
			console.log(okService);
			// Param ?
			if ('cat_id' in params) {
				var cat_id = params['cat_id'];
				// DB query
				var query = "SELECT * FROM PIY_CATALOGS WHERE id = :id";
				console.log('query : ' + query);
				console.log('id : ' + cat_id);

				// Execute query
				db.execute(query, [cat_id],
					function(err, result) {
						console.log(result);
						if (err) {
							// Error
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
						} else if (!result.rows[0]) {
							// Empty
							console.log(koFound);
							res.write('<p>' + koFound + '</p>');
							res.write('</body></html>');
							res.end();
						} else {
							// Success
							for (var i = 0; i < result.metaData.length; i++) {
								var col = result.metaData[i].name;
								if (!result.rows[0][i]) {
									var val = '';
								} else {
									var val = result.rows[0][i].toString();
								}
								res.write('<p>' + col + ' : ' + val + '</p>');
							}
							res.write('</body></html>');
							res.end();
						}
					}
				);
			} else {
				console.log(koParam);
				res.write('<p>' + koParam + '</p>');
				res.write('</body></html>');
				res.end();
			}

		}
		// Create Catalog
		else if (service == '/createCatalog') {
			console.log(okService);
			// Param ?
			var allOk = true;
			// ID
			var id = null;
			if ('id' in params) {
				id = params['id'];
			} else {
				allOk = false;
			}
			// LABEL
			var label = null;
			if ('label' in params) {
				var label = params['label'];
			} else {
				allOk = false;
			}
			// FK_STD_SPEC
			var spec = null;
			if ('spec' in params) {
				var spec = params['spec'];
			} else {
				allOk = false;
			}
			if (allOk) {
				// DB query
				var query = "INSERT INTO PIY_CATALOGS  (ID, LABEL, FK_STD_SPEC) VALUES (:id, :label, :spec)";
				console.log('query : ' + query);
				console.log('id : ' + id + ' label : ' + label + ' spec : ' + spec);

				// Execute query
				db.execute(query, [id, label, spec],
					function(err, result) {
						console.log(result);
						if (err) {
							// Error
							console.error(err);
							res.write('<p>' + err + '</p>');
							res.write('</body></html>');
							res.end();
						} else {
							// Succes
							console.log(okSuccess);
							db.commit();
							console.log(okCommit);
							res.write('<p>' + okCreation + '</p>');
							res.write('</body></html>');
							res.end();
						}
					}
				);
			} else {
				console.log(koParam);
				res.write('<p>' + koParam + '</p>');
				res.write('</body></html>');
				res.end();
			}
		} else {
			console.log(koService);
			res.write('<p>' + koService + '</p>');
			res.write('</body></html>');
			res.end();
		}
	} else {
		res.writeHead(404, {"Content-Type": "text/plain"});
		console.log(koPath);
		res.write(koPath);
		res.end();
	}
});

server.listen(8081);